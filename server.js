var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000;

var path = require('path');

var cors = require('cors');
app.use(cors());

app.use(express.static(__dirname + '/build/default'));

app.listen(port);

console.log('app polymer con fachada NodeJS escuchando en el puerto: ' + port);